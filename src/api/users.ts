import api from './api'
import { Axios, AxiosResponse } from 'axios'

interface IUsersApi {
  getUsers: () => Promise<AxiosResponse<IUser, Error>>
  addUser: (params: IUser) => Promise<AxiosResponse<IUser, Error>>
  deleteUser: (id: number) => Promise<AxiosResponse<IUser, Error>>
  editUser: (id: number, params: IUser) => Promise<AxiosResponse<IUser, Error>>
  getUser: (id: number) => Promise<AxiosResponse<IUser, Error>>
}

interface IUser {
  id?: number,
  name: string,
  surname: string,
  age: number
}

const usersApi: IUsersApi | { axios: Axios } = {
  getUsers() {
    return api.get('/users')
      .then(({ data }) => data)
      .catch(error => Promise.reject(error))
  },

  addUser(params) {
    return api.post('/users', params)
      .then(({ data }) => data)
      .catch(error => Promise.reject(error))
  },

  getUser(id) {
    return api.get('/users/' + id)
      .then(({ data }) => data)
      .catch(error => Promise.reject(error))
  },

  editUser(id, params) {
    return api.patch('/users/' + id, params)
      .then(({ data }) => data)
      .catch(error => Promise.reject(error))
  },

  deleteUser(id) {
    console.dir(id)
    return api.delete('/users/' + id)
      .then(({ data }) => data)
      .catch(error => Promise.reject(error))
  }
}

export default usersApi
