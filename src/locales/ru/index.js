export default {
  footer: {
    copyright: '© metalhead, 2022. Все права защищены'
  },

  pages: {
    new_user: 'Добавить пользователя',
    remove: 'Удалить',
    cancel: 'Отмена',
    submit: 'Сохранить',
    name: 'Имя:',
    surname: 'Фамилия:',
    age: 'Возраст:'
  }
}
