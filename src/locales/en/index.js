export default {
  footer: {
    copyright: '© metalhead, 2022. All rights reserved'
  },

  pages: {
    new_user: 'Add user',
    remove: 'Remove',
    cancel: 'Cancel',
    submit: 'Save',
    name: 'Name:',
    surname: 'Surname:',
    age: 'Age:'
  }
}
